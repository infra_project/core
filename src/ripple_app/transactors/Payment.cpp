//------------------------------------------------------------------------------
/*
    This file is part of rippled: https://github.com/ripple/rippled
    Copyright (c) 2012, 2013 Ripple Labs Inc.

    Permission to use, copy, modify, and/or distribute this software for any
    purpose  with  or without fee is hereby granted, provided that the above
    copyright notice and this permission notice appear in all copies.

    THE  SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
    WITH  REGARD  TO  THIS  SOFTWARE  INCLUDING  ALL  IMPLIED  WARRANTIES  OF
    MERCHANTABILITY  AND  FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
    ANY  SPECIAL ,  DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
    WHATSOEVER  RESULTING  FROM  LOSS  OF USE, DATA OR PROFITS, WHETHER IN AN
    ACTION  OF  CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
    OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*/
//==============================================================================

#include "../rpc/RPCHandler.h"

namespace ripple {

	 
#define MAX_PATH_SIZE 6
#define MAX_NUM_PATHS 6

TER PaymentTransactor::doApply ()
{
    // Ripple if source or destination is non-native or if there are paths.
    std::uint32_t const uTxFlags = mTxn.getFlags ();
	//bool const bPartialPayment = is_bit_set(uTxFlags, tfPartialPayment);
    bool const bLimitQuality = is_bit_set (uTxFlags, tfLimitQuality);
    bool const bNoRippleDirect = is_bit_set (uTxFlags, tfNoRippleDirect);
    bool const bPaths = mTxn.isFieldPresent (sfPaths);
    bool const bMax = mTxn.isFieldPresent (sfSendMax);
    uint160 uDstAccountID = mTxn.getFieldAccount160 (sfDestination);
    STAmount const saDstAmount = mTxn.getFieldAmount (sfAmount);
    STAmount const saMaxAmount = bMax
                                   ? mTxn.getFieldAmount (sfSendMax)
                                   : saDstAmount.isNative ()
                                   ? saDstAmount
                                   : STAmount (saDstAmount.getCurrency (),
                                        mTxnAccountID,
                                        saDstAmount.getMantissa (),
                                        saDstAmount.getExponent (),
                                        saDstAmount < zero);
    uint160 const uSrcCurrency = saMaxAmount.getCurrency ();
    uint160 const uDstCurrency = saDstAmount.getCurrency ();
    bool const bSTRDirect = uSrcCurrency.isZero () && uDstCurrency.isZero ();

	
    m_journal.trace <<
        "saMaxAmount=" << saMaxAmount.getFullText () <<
        " saDstAmount=" << saDstAmount.getFullText ();

    if (!saDstAmount.isLegalNet () || !saMaxAmount.isLegalNet ())
        return temBAD_AMOUNT;

    if (uTxFlags & tfPaymentMask)
    {
        m_journal.trace <<
            "Malformed transaction: Invalid flags set.";

        return temINVALID_FLAG;
    }
    else if (!uDstAccountID)
    {
        m_journal.trace <<
            "Malformed transaction: Payment destination account not specified.";

        return temDST_NEEDED;
    }
    else if (bMax && saMaxAmount <= zero)
    {
        m_journal.trace <<
            "Malformed transaction: bad max amount: " << saMaxAmount.getFullText ();

        return temBAD_AMOUNT;
    }
    else if (saDstAmount <= zero)
    {
        m_journal.trace <<
            "Malformed transaction: bad dst amount: " << saDstAmount.getFullText ();

        return temBAD_AMOUNT;
    }
    else if (CURRENCY_BAD == uSrcCurrency || CURRENCY_BAD == uDstCurrency)
    {
        m_journal.trace <<
            "Malformed transaction: Bad currency.";

        return temBAD_CURRENCY;
    }
    else if (mTxnAccountID == uDstAccountID && uSrcCurrency == uDstCurrency && !bPaths)
    {
        m_journal.trace <<
            "Malformed transaction: Redundant transaction:" <<
            " src=" << to_string (mTxnAccountID) <<
            " dst=" << to_string (uDstAccountID) <<
            " src_cur=" << to_string (uSrcCurrency) <<
            " dst_cur=" << to_string (uDstCurrency);

        return temREDUNDANT;
    }
    else if (bMax && saMaxAmount == saDstAmount && saMaxAmount.getCurrency () == saDstAmount.getCurrency ())
    {
        m_journal.trace <<
            "Malformed transaction: Redundant SendMax.";

        return temREDUNDANT_SEND_MAX;
    }
    else if (bSTRDirect && bMax)
    {
        m_journal.trace <<
            "Malformed transaction: SendMax specified for STR to STR.";

        return temBAD_SEND_STR_MAX;
    }
    else if (bSTRDirect && bPaths)
    {
        m_journal.trace <<
            "Malformed transaction: Paths specified for STR to STR.";

        return temBAD_SEND_STR_PATHS;
    }
    else if (bSTRDirect && bLimitQuality)
    {
        m_journal.trace <<
            "Malformed transaction: Limit quality specified for STR to STR.";

        return temBAD_SEND_STR_LIMIT;
    }
    else if (bSTRDirect && bNoRippleDirect)
    {
        m_journal.trace <<
            "Malformed transaction: No ripple direct specified for STR to STR.";

        return temBAD_SEND_STR_NO_DIRECT;
    }

	SLE::pointer sleDst = nullptr;
	if (!mTxn.isCrossGateway())
	{
		sleDst = (mEngine->entryCache(
			ltACCOUNT_ROOT, Ledger::getAccountRootIndex(uDstAccountID)));
	}
	else
	{
		uDstAccountID = mTxn.getFieldAccount160(sfGateway);
		sleDst = (mEngine->entryCache(
			ltACCOUNT_ROOT, Ledger::getAccountRootIndex(uDstAccountID)));
	}
	

    if (!sleDst)
    {
        // Destination account does not exist.

		if (mTxn.isCrossGateway())
		{
			m_journal.trace <<
				"Delay transaction: Destination account does not exist.";

			// Another transaction could create the account and then this transaction would succeed.
			return tecNO_GATEWAY_DST;
		}

        if (!saDstAmount.isNative ())
        {
            m_journal.trace <<
                "Delay transaction: Destination account does not exist.";

            // Another transaction could create the account and then this transaction would succeed.
            return tecNO_DST;
        }
        
        // Note: Reserve is not scaled by load.
        else if (saDstAmount.getNValue () < mEngine->getLedger ()->getReserve (0))
        {
            m_journal.trace <<
                "Delay transaction: Destination account does not exist. " <<
                "Insufficient payment to create account.";

            // Another transaction could create the account and then this
            // transaction would succeed.
            return tecNO_DST_INSUF_STR;
        }

        // Create the account.
        sleDst = mEngine->entryCreate (
            ltACCOUNT_ROOT, Ledger::getAccountRootIndex (uDstAccountID));

        sleDst->setFieldAccount (sfAccount, uDstAccountID);
        sleDst->setFieldU32 (sfSequence, 1);
    }
    else if ((sleDst->getFlags () & lsfRequireDestTag) && !mTxn.isFieldPresent (sfDestinationTag))
    {
        m_journal.trace <<
            "Malformed transaction: DestinationTag required.";

        return tefDST_TAG_NEEDED;
    }
    else
    {
        mEngine->entryModify (sleDst);
    }

    TER terResult;
    // XXX Should bMax be sufficient to imply ripple?
    bool const bRipple = bPaths || bMax || !saDstAmount.isNative ();

    if (bRipple)
    {
        // Ripple payment

        STPathSet spsPaths = mTxn.getFieldPathSet (sfPaths);
        std::vector<PathState::pointer> vpsExpanded;
        STAmount saMaxAmountAct;
        STAmount saDstAmountAct;

        try
        {
			bool tooManyPaths = false;
			if (spsPaths.size() > MAX_NUM_PATHS) tooManyPaths = true;
			else if (! LedgerDump::enactHistoricalQuirk (QuirkLongPaymentPaths))
			{
				for (auto const& path : spsPaths)
				{
					if (path.size() > MAX_PATH_SIZE)
					{
						tooManyPaths = true;
						break;
					}
				}
			}

			
			terResult = tooManyPaths
				? telBAD_PATH_COUNT // Too many paths for proposed ledger.
				: RippleCalc::rippleCalc(
				mEngine->view(),
				saMaxAmountAct,
				saDstAmountAct,
				vpsExpanded,
				saMaxAmount,
				saDstAmount,
				uDstAccountID,
				mTxnAccountID,
				spsPaths,
				false,
				bLimitQuality,
				bNoRippleDirect, // Always compute for finalizing ledger.
				false, // Not standalone, delete unfundeds.
				is_bit_set(mParams, tapOPEN_LEDGER));

			if (isTerRetry(terResult))
				terResult = tecPATH_DRY;

			if ((tesSUCCESS == terResult) && (saDstAmountAct != saDstAmount))
				mEngine->view().setDeliveredAmount(saDstAmountAct);
			
        }
        catch (std::exception const& e)
        {
            m_journal.trace <<
                "Caught throw: " << e.what ();

            terResult   = tefEXCEPTION;
        }
    }
    else
    {
        // Direct STR payment.

        std::uint32_t const uOwnerCount (mTxnAccount->getFieldU32 (sfOwnerCount));
        std::uint64_t const uReserve (mEngine->getLedger ()->getReserve (uOwnerCount));

        // Make sure have enough reserve to send. Allow final spend to use reserve for fee.
        if (mPriorBalance < saDstAmount + std::max(uReserve, mTxn.getTransactionFee ().getNValue ()))
        {
            // Vote no. However, transaction might succeed, if applied in a different order.
            m_journal.trace << "Delay transaction: Insufficient funds: " <<
                " " << mPriorBalance.getText () <<
                " / " << (saDstAmount + uReserve).getText () <<
                " (" << uReserve << ")";

            terResult   = tecUNFUNDED_PAYMENT;
        }
        else
        {
            mTxnAccount->setFieldAmount (sfBalance, mSourceBalance - saDstAmount);
            sleDst->setFieldAmount (sfBalance, sleDst->getFieldAmount (sfBalance) + saDstAmount);

            // re-arm the password change fee if we can and need to
            if (LedgerDump::enactHistoricalQuirk (QuirkUseLsfPasswordSpent) &&
                (sleDst->getFlags () & lsfPasswordSpent))
                sleDst->clearFlag (lsfPasswordSpent);

            terResult = tesSUCCESS;
        }
    }

    std::string strToken;
    std::string strHuman;

    if (transResultInfo (terResult, strToken, strHuman))
    {
        m_journal.trace <<
            strToken << ": " << strHuman;
    }
    else
    {
        assert (false);
    }

	if (mTxn.isCrossGateway() && terResult == tesSUCCESS)
	{
		std::map<RippleAddress, Config::Partner>::iterator it = getConfig().PARTNERS_ENDPOINTS.find(mTxn.getGatewayAccount());
		if (it == getConfig().PARTNERS_ENDPOINTS.end())
		{
			m_journal.error << "No endpoint for partner with address: " << mTxn.getGatewayAccount().humanAccountID();
		}
		Config::Partner partner = it->second;
		TER extResult = externalCrossTx(mTxn.getTransactionID(), partner);
		if (extResult == tecTX_NOT_FOUND)
		{
			STAmount externalAmount(saDstAmount);
			if (!externalAmount.isNative())
			{
				externalAmount.setIssuer(mTxn.getGatewayAccount().getAccountID());
			}
			while (true) {
				extResult = externalSubmit(getConfig().GATEWAY_ACCOUNT, mTxn.getFieldAccount160(sfDestination), mTxn.getTransactionID(), saDstAmount, partner);
				if (extResult != tefPAST_SEQ)
					break;
			}
			if (extResult != tesSUCCESS)
				return extResult;
			extResult = externalCrossTx(mTxn.getTransactionID(), partner);
		}
		return extResult;
	}

    return terResult;
}

TER PaymentTransactor::getBlob(Config::Partner partner, RippleAddress from, uint160 to, uint256 partnerTrId, STAmount amount, std::string& blob)
{
	Json::Value param;
	param["secret"] = from.humanSeed();
	Json::Value txJson;
	txJson["TransactionType"] = "Payment";
	txJson["Account"] = RippleAddress::createAccountPublic(from).humanAccountID();
	txJson["Destination"] = RippleAddress::createHumanAccountID(to);
	txJson["Amount"] = amount.getJson(0);
	txJson["PartnerTransactionHash"] = to_string(partnerTrId);
	txJson["Sequence"] = getSequence(partner, from);
	param["tx_json"] = txJson;
	param["offline"] = true;
	RPCHandler  h(&getApp().getOPs());
	Application::ScopedLockType lock(getApp().getMasterLock());
	Resource::Charge fee(Resource::feeReferenceRPC);
	m_journal.warning << "params: " << param.toStyledString();
	Json::Value response = h.doSign(param, fee, lock);
	m_journal.warning << "response: " << response.toStyledString();
	if (response.isMember("tx_blob"))
	{
		blob = response["tx_blob"].asString();
		return tesSUCCESS;
	}
	m_journal.error << "Failde to sign failed: " << response.asString();
	return tecPARTNER_ERROR;
}

uint32_t PaymentTransactor::getSequence(Config::Partner partner, RippleAddress account)
{
	Json::Value request;
	request["method"] = "account_info";
	Json::Value param;
	param["account"] = RippleAddress::createAccountPublic(account).humanAccountID();
	Json::Value params(Json::ValueType::arrayValue);
	params[(unsigned int)0] = param;
	request["params"] = params;
	RPCHandler  h(&getApp().getOPs());
	Application::ScopedLockType lock(getApp().getMasterLock());
	Json::Value response = h.doExternalTransaction(partner.getUrl(), request, lock);
	if (response.isMember("result"))
	{
		auto result = response["result"];
		if (result.isMember("account_data"))
		{
			auto accountData = result["account_data"];
			if (accountData.isMember("Sequence"))
			{
				return accountData["Sequence"].asUInt();
			}
		}
	}
	return 0;
}

TER PaymentTransactor::externalSubmit(RippleAddress from, uint160 to, uint256 partnerTrId, STAmount amount, Config::Partner partner)
{
	std::string blob;
	TER result = getBlob(partner, from, to, partnerTrId, amount, blob);
	if (result != tesSUCCESS)
	{
		return result;
	}
	Json::Value request;
	request["method"] = "submit";
	Json::Value param;
	param["tx_blob"] = blob;
	Json::Value params(Json::ValueType::arrayValue);
	params[(unsigned int)0] = param;
	request["params"] = params;
	RPCHandler  h(&getApp().getOPs());
	Application::ScopedLockType lock(getApp().getMasterLock());
	
	Json::Value response = h.doExternalTransaction(partner.getUrl(), request, lock);
	if (response.isMember("result"))
	{
		Json::Value result = response["result"];
		if (result.isMember("engine_result"))
		{
			if (result["engine_result"].isString())
			{
				std::string engineResult = result["engine_result"].asString();
				return transToken(engineResult);
			}
		}
	}

	m_journal.error << "Submit to partner " << partner.getName() << " at " << partner.getUrl() << " failed: " << response.toStyledString();
	return tecPARTNER_ERROR;
}

TER PaymentTransactor::externalCrossTx(uint256 const& crossId, Config::Partner partner)
{
	Json::Value request;
	request["method"] = "tx";
	Json::Value param;
	param["transaction"] = to_string(crossId);
	param["cross"] = true;
	Json::Value params(Json::ValueType::arrayValue);
	params[(unsigned int)0] = param;
	request["params"] = params;
	RPCHandler  h(&getApp().getOPs());
	Application::ScopedLockType lock(getApp().getMasterLock());
	m_journal.warning << request.toStyledString();
	Json::Value response = h.doExternalTransaction(partner.getUrl(), request, lock);
	m_journal.warning << response.toStyledString();
	if (response.isMember("result"))
	{
		Json::Value result = response["result"];
		if (result.isMember("meta"))
		{
			Json::Value meta = result["meta"];
			if (meta.isMember("TransactionResult"))
			{
				std::string trResult = meta["TransactionResult"].asString();
				return transToken(trResult);
			}
		}
		else if (result.isMember("error"))
		{
			if (result["error"].asString() == "txnNotFound")
			{
				return tecTX_NOT_FOUND;
			}
		}
	}

	m_journal.error << "Tx to partner " << partner.getName() << " at " << partner.getUrl() << " failed: " << response.toStyledString();
	return tecPARTNER_ERROR;
}

}

