//------------------------------------------------------------------------------
/*
    This file is part of rippled: https://github.com/ripple/rippled
    Copyright (c) 2012, 2013 Ripple Labs Inc.

    Permission to use, copy, modify, and/or distribute this software for any
    purpose  with  or without fee is hereby granted, provided that the above
    copyright notice and this permission notice appear in all copies.

    THE  SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
    WITH  REGARD  TO  THIS  SOFTWARE  INCLUDING  ALL  IMPLIED  WARRANTIES  OF
    MERCHANTABILITY  AND  FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
    ANY  SPECIAL ,  DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
    WHATSOEVER  RESULTING  FROM  LOSS  OF USE, DATA OR PROFITS, WHETHER IN AN
    ACTION  OF  CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
    OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*/
//==============================================================================

#ifndef RIPPLE_TX_PAYMENT_H_INCLUDED
#define RIPPLE_TX_PAYMENT_H_INCLUDED

namespace ripple {

class PaymentTransactorLog;

template <> char const* LogPartition::getPartitionName <PaymentTransactorLog> ()
{
    return "Tx/Payment";
}

class PaymentTransactor : public Transactor
{
public:
    PaymentTransactor (
        SerializedTransaction const& txn,
        TransactionEngineParams params,
        TransactionEngine* engine)
        : Transactor (
            txn,
            params,
            engine,
            LogPartition::getJournal <PaymentTransactorLog> ())
    {

    }

    TER doApply ();

private:

	TER externalCrossTx(uint256 const& crossId, Config::Partner partner);

	TER externalSubmit(RippleAddress from, uint160 to, uint256 partnerTrId, STAmount amount, Config::Partner partner);

	TER getBlob(Config::Partner partner, RippleAddress from, uint160 to, uint256 partnerTrId, STAmount amount, std::string& blob);

	uint32_t getSequence(Config::Partner partner, RippleAddress account);
};

}

#endif
